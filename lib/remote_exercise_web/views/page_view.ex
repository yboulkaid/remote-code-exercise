defmodule RemoteExerciseWeb.PageView do
  use RemoteExerciseWeb, :view

  def render("index.json", %{users_and_timestamp: users_and_timestamp}) do
    %{
      timestamp: users_and_timestamp.last_queried_at,
      users:
        Enum.map(
          users_and_timestamp.users,
          fn user -> %{id: user.id, points: user.points} end
        )
    }
  end
end
