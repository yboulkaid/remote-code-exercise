defmodule RemoteExerciseWeb.PageController do
  use RemoteExerciseWeb, :controller

  def index(conn, _params) do
    users_and_timestamp = RemoteExercise.PointManager.get_users(PointManager)

    render(conn, "index.json", users_and_timestamp: users_and_timestamp)
  end
end
