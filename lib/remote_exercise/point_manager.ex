defmodule RemoteExercise.PointManager do
  import Ecto.Query

  use GenServer

  def start_link(arg) do
    GenServer.start_link(
      __MODULE__,
      %{min_number: Enum.random(0..100), last_queried_at: nil},
      arg
    )
  end

  def get_users(pid) do
    GenServer.call(pid, :get_users)
  end

  @impl true
  def init(args) do
    :timer.send_interval(1_000 * 60, :update_points)
    {:ok, args}
  end

  @impl true
  def handle_info(:update_points, state) do
    update_all_user_points()
    new_min_number = Enum.random(0..100)

    {:noreply, %{state | min_number: new_min_number}}
  end

  @impl true
  def handle_call(:get_users, _from, state) do
    users = get_two_users_with_more_points_than(state.min_number)

    {:reply, %{users: users, last_queried_at: state[:last_queried_at]},
     %{state | last_queried_at: DateTime.utc_now()}}
  end

  defp get_two_users_with_more_points_than(min_number) do
    query = from u in RemoteExercise.User, where: u.points >= ^min_number, limit: 2

    RemoteExercise.Repo.all(query)
  end

  defp update_all_user_points do
    from(u in RemoteExercise.User,
      update: [set: [points: fragment("floor(random() * 101)::int")]]
    )
    |> RemoteExercise.Repo.update_all([])
  end
end
