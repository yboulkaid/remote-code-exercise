# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     RemoteExercise.Repo.insert!(%RemoteExercise.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

now =
  NaiveDateTime.utc_now()
  |> NaiveDateTime.truncate(:second)

batch =
  Enum.map(
    1..1_000,
    fn _i ->
      %{inserted_at: now, updated_at: now}
    end
  )

Enum.map(
  1..1_000,
  fn _i ->
    RemoteExercise.Repo.insert_all(RemoteExercise.User, batch)
  end
)
