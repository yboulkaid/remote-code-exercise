# Backend code exercise - Remote

This is an implementation of the [backend code
exercise](https://www.notion.so/Backend-code-exercise-d7df215ccb6f4d87a3ef865506763d50).

## Notes:

**Seed performance**

To seed the data with a million users in an efficient manner, we insert 1000
batches of 1000 users using the `insert_all` function. This is much more
efficient than inserting users one by one.

**Point update performance**

To update points for all users every minute, we do the work directly in
postgres rather than doing it in the application layer. This allows updates to
take seconds rather than minutes.

**Code formatting and idiomatic code**

The code has been formatted using `mix format`, but I'm not sure if it's
idiomatic as I have limited experience writing Elixir code.

**Commit history**

Commits are small and can be used to retrace the process of working through the
exercise. They are however not to be considered as commits that would make it
into a production codebase, as they lack proper messages and context. 

In a real-world setting they would be squashed into larger chunks with
descriptive messages.

**Testing**

There are both unit and integration tests. These do not cover all aspects of
what I would test in a real-world environment but show enough about my approach
to testing: small focused unit tests and larger integration tests that cover
behaviors for entire flows.

Some aspects that were not covered in this exercise in order to save time are:
- Testing the value of the timestamps through e.g. time-travel.
- Testing the `:timer.send_interval` behavior.
- Making the tests more robust to:
  - user points staying the same after randomization. This would fail the tests
    as they are written, but is a possible (though unlikely) case in the real world.
  - testing that the timestamps saved correspond to the last time the message was received.

## Instructions:

To start your Phoenix server:

- Install dependencies with `mix deps.get`
- Create, migrate and seed your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server`.

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To run tests, run `mix test`.
