defmodule RemoteExerciseWeb.PageControllerTest do
  use RemoteExerciseWeb.ConnCase

  test "GET /", %{conn: conn} do
    RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 1})
    RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 50})
    RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 99})

    # Set state explicitely.
    :sys.replace_state(PointManager, fn state -> %{state | min_number: 49} end)

    # First call, should get two users but no timestamp.
    conn = get(conn, "/")
    body = json_response(conn, 200)
    user_points = Enum.map(body["users"], fn user -> user["points"] end)

    assert body["timestamp"] == nil
    assert user_points == [50, 99]

    # First call, should get the same two users and a timestamp.
    conn = get(conn, "/")
    body = json_response(conn, 200)
    user_points = Enum.map(body["users"], fn user -> user["points"] end)

    assert body["timestamp"] != nil
    assert user_points == [50, 99]

    # Simulate the periodic point update.
    send(PointManager, :update_points)

    # Third call, should get both a timestamp and users with different points.
    conn = get(conn, "/")
    body = json_response(conn, 200)
    user_points = Enum.map(body["users"], fn user -> user["points"] end)

    assert user_points != [50, 99]
    assert body["timestamp"] != nil
  end
end
