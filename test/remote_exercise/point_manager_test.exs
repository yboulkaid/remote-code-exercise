defmodule RemoteExercise.PointManagerTest do
  use RemoteExercise.DataCase

  require Assertions
  import Assertions, only: [assert_lists_equal: 2]

  setup do
    point_manager = start_supervised!(RemoteExercise.PointManager)
    %{point_manager: point_manager}
  end

  describe "get_users/1" do
    test "gets two users with more points than its `min_number`", %{point_manager: point_manager} do
      _user1 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 1})
      user2 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 50})
      user3 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 99})
      :sys.replace_state(point_manager, fn state -> %{state | min_number: 49} end)

      result = RemoteExercise.PointManager.get_users(point_manager).users

      assert_lists_equal(result, [user2, user3])
    end

    test "gets a single user if only one has the required amount of points", %{
      point_manager: point_manager
    } do
      _user1 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 1})
      user2 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 99})
      :sys.replace_state(point_manager, fn state -> %{state | min_number: 49} end)

      result = RemoteExercise.PointManager.get_users(point_manager).users

      assert_lists_equal(result, [user2])
    end

    test "gets at most 2 users", %{point_manager: point_manager} do
      _user1 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 10})
      _user2 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 50})
      _user3 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 99})
      :sys.replace_state(point_manager, fn state -> %{state | min_number: 1} end)

      result = RemoteExercise.PointManager.get_users(point_manager).users

      assert length(result) == 2
    end

    test "refreshes the `last_queried_at` timestamp", %{point_manager: point_manager} do
      :sys.replace_state(point_manager, fn state -> %{state | last_queried_at: nil} end)

      # We need to get users twice to override the `nil`
      RemoteExercise.PointManager.get_users(point_manager).last_queried_at
      result = RemoteExercise.PointManager.get_users(point_manager).last_queried_at

      assert result != nil
    end
  end

  describe "Receiving the `update_points` message" do
    test "it updates the points for all users", %{point_manager: point_manager} do
      user1 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 50})
      user2 = RemoteExercise.Repo.insert!(%RemoteExercise.User{points: 50})
      :sys.replace_state(point_manager, fn state -> %{state | min_number: 0} end)

      send(PointManager, :update_points)

      new_user_1 = RemoteExercise.Repo.get(RemoteExercise.User, user1.id)
      new_user_2 = RemoteExercise.Repo.get(RemoteExercise.User, user2.id)
      assert new_user_1.points != 50
      assert new_user_2.points != 50
    end

    test "refreshes the `min_number`", %{point_manager: point_manager} do
      :sys.replace_state(point_manager, fn state -> %{state | min_number: 0} end)

      send(PointManager, :update_points)

      new_min_number = :sys.get_state(PointManager)
      assert new_min_number != 0
    end
  end
end
